Ansible -- easy build & push docker images
======================

`Ansible` is a tool to ease the deployment of services across multiple servers. We use it here to provide an easy workflow to build and push the docker images associated with the `rex-dri` project; from a remote server.

> :information_desk_person: this is clearly not useful if you have a very good upload bandwidth.

## How to

Make sure you can connect to the remote server of your choice :smile:

* Install `ansible-galaxy` dependencies:
    - `ansible-galaxy install -r requirements.yml`

* Setup env variables:
    - Change the `IP` of the remote host server in `inventory.yml`
    - Copy `vars.ex.yml` to `vars.yml` and set the correct variables.

* Run:

```bash
ansible-playbook -i inventory.yml playbook.yml 
```

:confetti_ball: