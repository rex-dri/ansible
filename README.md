Ansible 
=========

`Ansible` is a tool to ease the deployment of services across multiple servers. We use it here to provide easy workflows.

## General setup:

* Install `ansible`:
    - Ubuntu like: `sudo apt-get update && sudo apt-get install ansible`
    - Fedora like: `sudo dnf install ansible`

## Available scripts

* [Build docker images and push it to gitlab registry](./build-docker)
